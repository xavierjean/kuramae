const kanjis = [
    {
        kanji: '一',
        strokes: 1,
        level: 'N5',
        frequency: 2,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひと',
                },
            ],
            onyomi: [
                {
                    'kana': 'イチ',
                },
                {
                    'kana': 'イッ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['hito, ichi'],
        translation: 'un',
    },
    {
        kanji: '二',
        strokes: 2,
        level: 'N5',
        frequency: 9,
        readings: {
            kunyomi: [
                {
                    'kana': 'ふた',
                },
                {
                    'kana': 'ふつ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ニ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['futa', 'futsu'],
        translation: 'deux',
    },
    {
        kanji: '三',
        strokes: 3,
        level: 'N5',
        frequency: 14,
        readings: {
            kunyomi: [
                {
                    'kana': 'みっ',
                },
            ],
            onyomi: [
                {
                    'kana': 'サン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['san'],
        translation: 'trois',
    },
    {
        kanji: '四',
        strokes: 5,
        level: 'N5',
        frequency: 47,
        readings: {
            kunyomi: [
                {
                    'kana': 'よん',
                },
                {
                    'kana': 'よっ',
                },
            ],
            onyomi: [
                {
                    'kana': 'シ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['yon', 'shi'],
        translation: 'quatre',
    },
    {
        kanji: '五',
        strokes: 4,
        level: 'N5',
        frequency: 31,
        readings: {
            kunyomi: [
                {
                    'kana': 'いつ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゴ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['itsu', 'go'],
        translation: 'cinq',
    },
    {
        kanji: '六',
        strokes: 4,
        level: 'N5',
        frequency: 93,
        readings: {
            kunyomi: [
                {
                    'kana': 'むっ',
                },
                {
                    'kana': 'むい',
                },
            ],
            onyomi: [
                {
                    'kana': 'ロク',
                },
                {
                    'kana': 'ロッ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mui', 'roku'],
        translation: 'six',
    },
    {
        kanji: '七',
        strokes: 2,
        level: 'N5',
        frequency: 115,
        readings: {
            kunyomi: [
                {
                    'kana': 'なな',
                },
                {
                    'kana': 'なの',
                },
            ],
            onyomi: [
                {
                    'kana': 'シチ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['nana', 'nano', 'shichi'],
        translation: 'sept',
    },
    {
        kanji: '八',
        strokes: 2,
        level: 'N5',
        frequency: 92,
        readings: {
            kunyomi: [
                {
                    'kana': 'やっ',
                },
                {
                    'kana': 'よう',
                },
            ],
            onyomi: [
                {
                    'kana': 'ハチ',
                },
                {
                    'kana': 'ハッ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['yo', 'you', 'hachi'],
        translation: 'huit',
    },
    {
        kanji: '九',
        strokes: 2,
        level: 'N5',
        frequency: 55,
        readings: {
            kunyomi: [
                {
                    'kana': 'ここの',
                },
            ],
            onyomi: [
                {
                    'kana': 'キュウ',
                },
                {
                    'kana': 'ク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kokono', 'kyu', 'kyuu', 'ku'],
        translation: 'neuf',
    },
    {
        kanji: '十',
        strokes: 2,
        level: 'N5',
        frequency: 8,
        readings: {
            kunyomi: [
                {
                    'kana': 'とう',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジュウ',
                },
                {
                    'kana': 'ジュっ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['to', 'tou', 'ju', 'juu'],
        translation: 'dix',
    },
    {
        kanji: '百',
        strokes: 6,
        level: 'N5',
        frequency: 163,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'ヒャク',
                },
                {
                    'kana': 'ビャク',
                },
                {
                    'kana': 'ピャク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['hyaku', 'byaku', 'pyaku'],
        translation: 'cent',
    },
    {
        kanji: '千',
        strokes: 3,
        level: 'N5',
        frequency: 195,
        readings: {
            kunyomi: [
                {
                    'kana': 'ち',
                },
            ],
            onyomi: [
                {
                    'kana': 'セン',
                },
                {
                    'kana': 'ゼン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['chi', 'sen', 'zen'],
        translation: 'mille',
    },
    {
        kanji: '万',
        strokes: 3,
        level: 'N5',
        frequency: 375,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'マン',
                },
                {
                    'kana': 'バン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['man', 'ban'],
        translation: 'dix mille',
    },
    {
        kanji: '日',
        strokes: 4,
        level: 'N5',
        frequency: 1,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひ',
                },
                {
                    'kana': 'び',
                },
                {
                    'kana': 'か',
                },
            ],
            onyomi: [
                {
                    'kana': 'ニチ',
                },
                {
                    'kana': '二',
                },
                {
                    'kana': 'ジツ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['hi', 'bi', 'ka', 'nichi', 'ni', 'jitsu'],
        translation: 'soleil, jour',
    },
    {
        kanji: '本',
        strokes: 5,
        level: 'N5',
        frequency: 10,
        readings: {
            kunyomi: [
                {
                    'kana': 'もと',
                },
            ],
            onyomi: [
                {
                    'kana': 'ホン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['moto', 'hon'],
        translation: 'origine, livre',
    },
    {
        kanji: '東',
        strokes: 8,
        level: 'N5',
        frequency: 37,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひがし',
                },
            ],
            onyomi: [
                {
                    'kana': 'トウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['higashi', 'to', 'tou'],
        translation: 'Est',
    },
    {
        kanji: '西',
        strokes: 6,
        level: 'N5',
        frequency: 259,
        readings: {
            kunyomi: [
                {
                    'kana': 'にし',
                },
            ],
            onyomi: [
                {
                    'kana': 'サイ',
                },
                {
                    'kana': 'セイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['nishi', 'sai', 'sei'],
        translation: 'Ouest',
    },
    {
        kanji: '北',
        strokes: 5,
        level: 'N5',
        frequency: 153,
        readings: {
            kunyomi: [
                {
                    'kana': 'きた',
                },
            ],
            onyomi: [
                {
                    'kana': 'ホク',
                },
                {
                    'kana': 'ホッ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kita', 'hoku'],
        translation: 'Nord',
    },
    {
        kanji: '南',
        strokes: 9,
        level: 'N5',
        frequency: 341,
        readings: {
            kunyomi: [
                {
                    'kana': 'みなみ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ナン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['minami', 'nan'],
        translation: 'Sud',
    },
    {
        kanji: '京',
        strokes: 8,
        level: 'N4',
        frequency: 74,
        readings: {
            kunyomi: [
                {
                    'kana': 'みやこ',
                },
            ],
            onyomi: [
                {
                    'kana': 'キョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['miyako', 'kyo', 'kyou'],
        translation: 'capitale',
    },
    {
        kanji: '国',
        strokes: 8,
        level: 'N5',
        frequency: 3,
        readings: {
            kunyomi: [
                {
                    'kana': 'くに',
                },
                {
                    'kana': 'ぐに',
                },
            ],
            onyomi: [
                {
                    'kana': 'コク',
                },
                {
                    'kana': 'ゴク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kuni', 'guni', 'koku', 'goku'],
        translation: 'pays',
    },
    {
        kanji: '海',
        strokes: 9,
        level: 'N4',
        frequency: 200,
        readings: {
            kunyomi: [
                {
                    'kana': 'うみ',
                },
            ],
            onyomi: [
                {
                    'kana': 'カイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['umi', 'kai'],
        translation: 'mer',
    },
    {
        kanji: '風',
        strokes: 9,
        level: 'N4',
        frequency: 558,
        readings: {
            kunyomi: [
                {
                    'kana': 'かぜ',
                },
            ],
            onyomi: [
                {
                    'kana': 'フウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kaze', 'fu', 'fuu'],
        translation: 'vent',
    },
    {
        kanji: '火',
        strokes: 4,
        level: 'N5',
        frequency: 574,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひ',
                },
            ],
            onyomi: [
                {
                    'kana': 'カ',
                },
            ],
        },
        romaji: ['hi', 'ka'],
        collections: [
            1,
        ],
        translation: 'feu',
    },
    {
        kanji: '土',
        strokes: 3,
        level: 'N5',
        frequency: 307,
        readings: {
            kunyomi: [
                {
                    'kana': 'つち',
                },
            ],
            onyomi: [
                {
                    'kana': 'ド',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['tsuchi', 'do'],
        translation: 'sol, terre',
    },
    {
        kanji: '水',
        strokes: 4,
        level: 'N5',
        frequency: 223,
        readings: {
            kunyomi: [
                {
                    'kana': 'みず',
                },
            ],
            onyomi: [
                {
                    'kana': 'スイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mizu', 'sui'],
        translation: 'eau',
    },
    {
        kanji: '時',
        strokes: 10,
        level: 'N5',
        frequency: 16,
        readings: {
            kunyomi: [
                {
                    'kana': 'とき',
                },
                {
                    'kana': 'どき',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['toki', 'doki', 'ji'],
        translation: 'temps',
    },
    {
        kanji: '空',
        strokes: 8,
        level: 'N4',
        frequency: 304,
        readings: {
            kunyomi: [
                {
                    'kana': 'そら',
                },
            ],
            onyomi: [
                {
                    'kana': 'クウ',
                },
            ],
        },
        romaji: ['sora', 'ku', 'kuu'],
        translation: 'ciel',
    },
    {
        kanji: '月',
        strokes: 4,
        level: 'N5',
        frequency: 23,
        readings: {
            kunyomi: [
                {
                    'kana': 'つき',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゲツ',
                },
                {
                    'kana': 'ガツ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['tsuki', 'getsu', 'gatsu'],
        translation: 'Lune',
    },
    {
        kanji: '山',
        strokes: 3,
        level: 'N5',
        frequency: 131,
        readings: {
            kunyomi: [
                {
                    'kana': 'やま',
                },
            ],
            onyomi: [
                {
                    'kana': 'サン',
                },
                {
                    'kana': 'ザン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['yama', 'san', 'zan'],
        translation: 'montagne',
    },
    {
        kanji: '川',
        strokes: 3,
        level: 'N5',
        frequency: 181,
        readings: {
            kunyomi: [
                {
                    'kana': 'かわ',
                },
            ],
            onyomi: [
                {
                    'kana': 'セン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kawa', 'sen'],
        translation: 'rivière',
    },
    {
        kanji: '木',
        strokes: 4,
        level: 'N5',
        frequency: 317,
        readings: {
            kunyomi: [
                {
                    'kana': 'き',
                },
            ],
            onyomi: [
                {
                    'kana': 'モク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ki', 'moku'],
        translation: 'arbre, bois',
    },
    {
        kanji: '花',
        strokes: 7,
        level: 'N4',
        frequency: 578,
        readings: {
            kunyomi: [
                {
                    'kana': 'はな',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['hana'],
        translation: 'fleur',
    },
    {
        kanji: '雨',
        strokes: 8,
        level: 'N5',
        frequency: 950,
        readings: {
            kunyomi: [
                {
                    'kana': 'あめ',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['ame'],
        translation: 'pluie',
    },
    {
        kanji: '金',
        strokes: 8,
        level: 'N5',
        frequency: 53,
        readings: {
            kunyomi: [
                {
                    'kana': 'かね',
                },
            ],
            onyomi: [
                {
                    'kana': 'キン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kane', 'kin'],
        translation: 'or, métal, argent',
    },
    {
        kanji: '雪',
        strokes: 11,
        level: 'N3',
        frequency: 1131,
        readings: {
            kunyomi: [
                {
                    'kana': 'ゆき',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['yuki'],
        translation: 'neige',
    },
    {
        kanji: '天',
        strokes: 4,
        level: 'N5',
        frequency: 512,
        readings: {
            kunyomi: [
                {
                    'kana': 'あまつ',
                },
                {
                    'kana': 'あめ',
                },
            ],
            onyomi: [
                {
                    'kana': 'テン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['amatsu', 'ame', 'ten'],
        translation: 'Ciel, paradis',
    },
    {
        kanji: '曜',
        strokes: 18,
        level: 'N4',
        frequency: 940,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'ヨウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['yo', 'you'],
        translation: 'jour de la semaine',
    },
    {
        kanji: '星',
        strokes: 9,
        level: 'N2',
        frequency: 844,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'セイ',
                },
            ],
        },
        romaji: ['sei'],
        translation: 'astre',
    },
    {
        kanji: '王',
        strokes: 4,
        level: 'N3',
        frequency: 684,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'オウ',
                },
            ],
        },
        romaji: ['o', 'ou'],
        translation: 'roi',
    },
    {
        kanji: '男',
        strokes: 7,
        level: 'N5',
        frequency: 240,
        readings: {
            kunyomi: [
                {
                    'kana': 'おとこ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ナン',
                },
                {
                    'kana': 'ダン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['otoko', 'nan', 'dan'],
        translation: 'homme, masculin',
    },
    {
        kanji: '女',
        strokes: 3,
        level: 'N5',
        frequency: 151,
        readings: {
            kunyomi: [
                {
                    'kana': 'おんあ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジョ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['onna', 'jo'],
        translation: 'femme, féminin',
    },
    {
        kanji: '子',
        strokes: 3,
        level: 'N5',
        frequency: 72,
        readings: {
            kunyomi: [
                {
                    'kana': 'こ',
                },
            ],
            onyomi: [
                {
                    'kana': 'シ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ko', 'shi'],
        translation: 'enfant',
    },
    {
        kanji: '私',
        strokes: 7,
        level: 'N4',
        frequency: 242,
        readings: {
            kunyomi: [
                {
                    'kana': 'わたし',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['watashi'],
        translation: 'je (moi), privé',
    },
    {
        kanji: '犬',
        strokes: 4,
        level: 'N4',
        frequency: 1326,
        readings: {
            kunyomi: [
                {
                    'kana': 'いぬ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ケン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['inu', 'ken'],
        translation: 'chien',
    },
    {
        kanji: '猫',
        strokes: 11,
        level: 'N3',
        frequency: 1702,
        readings: {
            kunyomi: [
                {
                    'kana': 'ねこ',
                },
            ],
            onyomi: [],
        },
        romaji: ['neko'],
        translation: 'chat',
    },
    {
        kanji: '鳥',
        strokes: 11,
        level: 'N4',
        frequency: 1043,
        readings: {
            kunyomi: [
                {
                    'kana': 'とり',
                },
            ],
            onyomi: [
                {
                    'kana': 'チョウ',
                }
            ],
        },
        collections: [
            1,
        ],
        romaji: ['tori', 'cho', 'chou'],
        translation: 'oiseau',
    },
    {
        kanji: '馬',
        strokes: 10,
        level: 'N3',
        frequency: 639,
        readings: {
            kunyomi: [
                {
                    'kana': 'うま',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['uma'],
        translation: 'cheval',
    },
    {
        kanji: '行',
        strokes: 6,
        level: 'N5',
        frequency: 20,
        readings: {
            kunyomi: [
                {
                    'kana': 'い',
                },
                {
                    'kana': 'おこな',
                },
            ],
            onyomi: [
                {
                    'kana': 'コウ',
                },
                {
                    'kana': 'ギョウ',
                },
                {
                    'kana': 'アン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['i', 'okona', 'ko', 'kou', 'gyo', 'gyou', 'an'],
        translation: 'déplacement, conduite, agissement',
    },
    {
        kanji: '住',
        strokes: 7,
        level: 'N4',
        frequency: 270,
        readings: {
            kunyomi: [
                {
                    'kana': 'す',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジュウ',
                },
            ],
        },
        romaji: ['su', 'ju', 'juu'],
        translation: 'demeure, résidence',
    },
    {
        kanji: '休',
        strokes: 6,
        level: 'N5',
        frequency: 642,
        readings: {
            kunyomi: [
                {
                    'kana': 'やす',
                },
            ],
            onyomi: [
                {
                    'kana': 'キョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['yasu', 'kyo', 'kyou'],
        translation: 'repos',
    },
    {
        kanji: '食',
        strokes: 9,
        level: 'N5',
        frequency: 328,
        readings: {
            kunyomi: [
                {
                    'kana': 'た',
                },
            ],
            onyomi: [
                {
                    'kana': 'ショク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ta', 'shoku'],
        translation: 'nourriture, manger',
    },
    {
        kanji: '飲',
        strokes: 12,
        level: 'N4',
        frequency: 969,
        readings: {
            kunyomi: [
                {
                    'kana': 'の',
                },
            ],
            onyomi: [
                {
                    'kana': 'イン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['no', 'in'],
        translation: 'boisson, boire',
    },
    {
        kanji: '分',
        strokes: 4,
        level: 'N5',
        frequency: 24,
        readings: {
            kunyomi: [
                {
                    'kana': 'わ',
                },
            ],
            onyomi: [
                {
                    'kana': 'フン',
                },
                {
                    'kana': 'ブン',
                },
                {
                    'kana': 'ブ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['wa', 'fun', 'bun', 'bu'],
        translation: 'part, portion',
    },
    {
        kanji: '来',
        strokes: 7,
        level: 'N5',
        frequency: 102,
        readings: {
            kunyomi: [
                {
                    'kana': 'く',
                },
                {
                    'kana': 'き',
                },
            ],
            onyomi: [
                {
                    'kana': 'ライ',
                }
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ku', 'rai'],
        translation: 'arriver, prochain, cause',
    },
    {
        kanji: '見',
        strokes: 7,
        level: 'N5',
        frequency: 22,
        readings: {
            kunyomi: [
                {
                    'kana': 'み',
                },
            ],
            onyomi: [
                {
                    'kana': 'ケン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mi', 'ken'],
        translation: 'voir, vision',
    },
    {
        kanji: '聞',
        strokes: 14,
        level: 'N5',
        frequency: 319,
        readings: {
            kunyomi: [
                {
                    'kana': 'き',
                },
            ],
            onyomi: [
                {
                    'kana': 'ブン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ki', 'fun'],
        translation: 'écouter, demander',
    },
    {
        kanji: '言',
        strokes: 7,
        level: 'N4',
        frequency: 83,
        readings: {
            kunyomi: [
                {
                    'kana': 'い',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゲン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['i', 'gen'],
        translation: 'dire, mot',
    },
    {
        kanji: '話',
        strokes: 13,
        level: 'N5',
        frequency: 134,
        readings: {
            kunyomi: [
                {
                    'kana': 'はな',
                },
                {
                    'kana': 'はなし',
                },
            ],
            onyomi: [
                {
                    'kana': 'ワ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['hana', 'hanashi', 'wa'],
        translation: 'parler, récit, conversation',
    },
    {
        kanji: '語',
        strokes: 14,
        level: 'N5',
        frequency: 301,
        readings: {
            kunyomi: [
                {
                    'kana': 'かた',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゴ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kata', 'go'],
        translation: 'mot, parole, langue',
    },
    {
        kanji: '読',
        strokes: 14,
        level: 'N5',
        frequency: 618,
        readings: {
            kunyomi: [
                {
                    'kana': 'よ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ドク',
                },
                {
                    'kana': 'トク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['yo', 'doku', 'toku'],
        translation: 'lecture',
    },
    {
        kanji: '書',
        strokes: 10,
        level: 'N5',
        frequency: 169,
        readings: {
            kunyomi: [
                {
                    'kana': 'か',
                },
            ],
            onyomi: [
                {
                    'kana': 'ショ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ka', 'sho'],
        translation: 'écriture',
    },
    {
        kanji: '知',
        strokes: 8,
        level: 'N4',
        frequency: 205,
        readings: {
            kunyomi: [
                {
                    'kana': 'し',
                },
            ],
            onyomi: [
                {
                    'kana': 'チ',
                },
            ],
        },
        romaji: ['shi', 'chi'],
        translation: 'savoir, sagesse',
    },
    {
        kanji: '思',
        strokes: 9,
        level: 'N4',
        frequency: 132,
        readings: {
            kunyomi: [
                {
                    'kana': 'おも',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['omo'],
        translation: 'penser',
    },
    {
        kanji: '教',
        strokes: 11,
        level: 'N4',
        frequency: 166,
        readings: {
            kunyomi: [
                {
                    'kana': 'おし',
                },
                {
                    'kana': 'おそ',
                },
            ],
            onyomi: [
                {
                    'kana': 'キョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['oshi', 'oso', 'kyo', 'kyou'],
        translation: 'enseignement, foi, doctrine',
    },
    {
        kanji: '歩',
        strokes: 8,
        level: 'N4',
        frequency: 554,
        readings: {
            kunyomi: [
                {
                    'kana': 'ある',
                },
            ],
            onyomi: [],
        },
        romaji: ['aru'],
        translation: 'marcher',
    },
    {
        kanji: '入',
        strokes: 2,
        level: 'N5',
        frequency: 56,
        readings: {
            kunyomi: [
                {
                    'kana': 'はい',
                },
                {
                    'kana': 'いり',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['hai', 'iri'],
        translation: 'entrée',
    },
    {
        kanji: '夕',
        strokes: 3,
        level: 'N4',
        frequency: 924,
        readings: {
            kunyomi: [
                {
                    'kana': 'ゆう',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['yu', 'yuu'],
        translation: 'soirée',
    },
    {
        kanji: '大',
        strokes: 3,
        level: 'N5',
        frequency: 7,
        readings: {
            kunyomi: [
                {
                    'kana': 'おお',
                },
            ],
            onyomi: [
                {
                    'kana': 'ダイ',
                },
                {
                    'kana': 'タイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['oo', 'dai', 'tai'],
        translation: 'grand',
    },
    {
        kanji: '小',
        strokes: 3,
        level: 'N5',
        frequency: 114,
        readings: {
            kunyomi: [
                {
                    'kana': 'ちい',
                },
            ],
            onyomi: [
                {
                    'kana': 'ショウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['chii'],
        translation: 'petit',
    },
    {
        kanji: '上',
        strokes: 3,
        level: 'N5',
        frequency: 35,
        readings: {
            kunyomi: [
                {
                    'kana': 'うえ',
                },
                {
                    'kana': 'あ',
                },
                {
                    'kana': 'のぼ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ue', 'a', 'nobo', 'jo', 'jou'],
        translation: 'le dessus',
    },
    {
        kanji: '下',
        strokes: 3,
        level: 'N5',
        frequency: 97,
        readings: {
            kunyomi: [
                {
                    'kana': 'した',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['shita'],
        translation: 'le dessous',
    },
    {
        kanji: '口',
        strokes: 3,
        level: 'N4',
        frequency: 284,
        readings: {
            kunyomi: [
                {
                    'kana': 'くち',
                },
                {
                    'kana': 'ぐち',
                },
            ],
            onyomi: [
                {
                    'kana': 'コウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kuchi', 'guchi', 'ko', 'kou'],
        translation: 'bouche',
    },
    {
        kanji: '円',
        strokes: 4,
        level: 'N5',
        frequency: 69,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'エン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['en'],
        translation: 'Yen, cercle',
    },
    {
        kanji: '今',
        strokes: 4,
        level: 'N5',
        frequency: 49,
        readings: {
            kunyomi: [
                {
                    'kana': 'いま',
                },
            ],
            onyomi: [
                {
                    'kana': 'コン',
                },
                {
                    'kana': 'コ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ima', 'kon', 'ko'],
        translation: 'maintenant',
    },
    {
        kanji: '中',
        strokes: 4,
        level: 'N5',
        frequency: 11,
        readings: {
            kunyomi: [
                {
                    'kana': 'なか',
                },
            ],
            onyomi: [
                {
                    'kana': 'チュウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['naka', 'chu', 'chuu'],
        translation: 'milieu, intérieur',
    },
    {
        kanji: '父',
        strokes: 4,
        level: 'N5',
        frequency: 646,
        readings: {
            kunyomi: [
                {
                    'kana': 'ちち',
                },
            ],
            onyomi: [
                {
                    'kana': 'フ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['chichi', 'fu'],
        translation: 'père',
    },
    {
        kanji: '牛',
        strokes: 4,
        level: 'N4',
        frequency: 1202,
        readings: {
            kunyomi: [
                {
                    'kana': 'うし',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['ushi'],
        translation: 'bovin',
    },
    {
        kanji: '田',
        strokes: 5,
        frequency: 90,
        level: 'N4',
        readings: {
            kunyomi: [
                {
                    'kana': 'た',
                },
                {
                    'kana': 'だ',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['ta', 'da'],
        translation: 'rizière',
    },
    {
        kanji: '生',
        strokes: 5,
        level: 'N5',
        frequency: 29,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'セイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['sei'],
        translation: 'vie',
    },
    {
        kanji: '白',
        strokes: 5,
        level: 'N5',
        frequency: 483,
        readings: {
            kunyomi: [
                {
                    'kana': 'しろ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ハク',
                },
                {
                    'kana': 'ハッ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['shiro', 'haku'],
        translation: 'blanc',
    },
    {
        kanji: '古',
        strokes: 5,
        level: 'N4',
        frequency: 509,
        readings: {
            kunyomi: [
                {
                    'kana': 'ふる',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['furu'],
        translation: 'ancienneté, vieillesse',
    },
    {
        kanji: '目',
        strokes: 5,
        level: 'N4',
        frequency: 76,
        readings: {
            kunyomi: [
                {
                    'kana': 'め',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['me'],
        translation: 'œil',
    },
    {
        kanji: '母',
        strokes: 5,
        level: 'N5',
        frequency: 570,
        readings: {
            kunyomi: [
                {
                    'kana': 'はは',
                },
            ],
            onyomi: [
                {
                    'kana': 'ボ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['haha', 'bo'],
        translation: 'mère',
    },
    {
        kanji: '年',
        strokes: 6,
        level: 'N5',
        frequency: 6,
        readings: {
            kunyomi: [
                {
                    'kana': 'とし',
                },
            ],
            onyomi: [
                {
                    'kana': 'ネン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['toshi', 'nen'],
        translation: 'année',
    },
    {
        kanji: '気',
        strokes: 6,
        level: 'N5',
        frequency: 113,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'キ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ki'],
        translation: 'esprit, air',
    },
    {
        kanji: '先',
        strokes: 6,
        level: 'N5',
        frequency: 173,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'セン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['sen'],
        translation: 'avance, qui précède',
    },
    {
        kanji: '文',
        strokes: 4,
        level: 'N4',
        frequency: 190,
        readings: {
            kunyomi: [
                {
                    'kana': 'ふみ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ブン',
                },
                {
                    'kana': 'モン',
                },
                {
                    'kana': 'モ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['fumi', 'bun', 'mon', 'mo'],
        translation: 'phrase, lettre',
    },
    {
        kanji: '字',
        strokes: 6,
        level: 'N4',
        frequency: 485,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'ジ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ji'],
        translation: 'caractère, lettre',
    },
    {
        kanji: '安',
        strokes: 6,
        level: 'N4',
        frequency: 144,
        readings: {
            kunyomi: [
                {
                    'kana': 'やす',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['yasu'],
        translation: 'bon marché, paisible',
    },
    {
        kanji: '早',
        strokes: 6,
        level: 'N4',
        frequency: 402,
        readings: {
            kunyomi: [
                {
                    'kana': 'はや',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['haya'],
        translation: 'tôt, rapide',
    },
    {
        kanji: '好',
        strokes: 6,
        level: 'N3',
        frequency: 423,
        readings: {
            kunyomi: [
                {
                    'kana': 'す',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['su'],
        translation: 'aimé, favorable, apprécié',
    },
    {
        kanji: '何',
        strokes: 7,
        level: 'N5',
        frequency: 340,
        readings: {
            kunyomi: [
                {
                    'kana': 'なん',
                },
                {
                    'kana': 'なに',
                },
            ],
            onyomi: [
                {
                    'kana': 'カ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['nan', 'nani', 'ka'],
        translation: 'quoi',
    },
    {
        kanji: '耳',
        strokes: 6,
        level: 'N3',
        frequency: 1328,
        readings: {
            kunyomi: [
                {
                    'kana': 'みみ',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['mimi'],
        translation: 'oreille',
    },
    {
        kanji: '町',
        strokes: 7,
        level: 'N4',
        frequency: 292,
        readings: {
            kunyomi: [
                {
                    'kana': 'まち',
                },
            ],
            onyomi: [
                {
                    'kana': 'チョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['machi', 'cho', 'chou'],
        translation: 'ville',
    },
    {
        kanji: '車',
        strokes: 7,
        level: 'N5',
        frequency: 333,
        readings: {
            kunyomi: [
                {
                    'kana': 'くるま',
                },
            ],
            onyomi: [
                {
                    'kana': 'シャ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kuruma', 'sha'],
        translation: 'véhicule, roue',
    },
    {
        kanji: '赤',
        strokes: 7,
        level: 'N4',
        frequency: 584,
        readings: {
            kunyomi: [
                {
                    'kana': 'あか',
                },
            ],
            onyomi: [],
        },
        romaji: ['aka'],
        translation: 'rouge',
    },
    {
        kanji: '体',
        strokes: 7,
        level: 'N4',
        frequency: 88,
        readings: {
            kunyomi: [
                {
                    'kana': 'からだ',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['karada'],
        translation: 'corps',
    },
    {
        kanji: '学',
        strokes: 8,
        level: 'N5',
        frequency: 63,
        readings: {
            kunyomi: [
                {
                    'kana': 'まな',
                },
            ],
            onyomi: [
                {
                    'kana': 'ガク',
                },
                {
                    'kana': 'ガッ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mana', 'gaku'],
        translation: 'étudier, étude',
    },
    {
        kanji: '店',
        strokes: 8,
        level: 'N4',
        frequency: 378,
        readings: {
            kunyomi: [
                {
                    'kana': 'みせ',
                },
            ],
            onyomi: [
                {
                    'kana': 'テン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mise', 'ten'],
        translation: 'magasin',
    },
    {
        kanji: '音',
        strokes: 9,
        level: 'N4',
        frequency: 491,
        readings: {
            kunyomi: [
                {
                    'kana': 'おと',
                },
            ],
            onyomi: [
                {
                    'kana': 'オン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['oto', 'on'],
        translation: 'son, bruit',
    },
    {
        kanji: '昼',
        strokes: 9,
        level: 'N4',
        frequency: 1115,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひる',
                },
            ],
            onyomi: [
                {
                    'kana': 'チュウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['hiru', 'chu', 'chuu'],
        translation: 'midi',
    },
    {
        kanji: '高',
        strokes: 10,
        level: 'N5',
        frequency: 65,
        readings: {
            kunyomi: [
                {
                    'kana': 'たか',
                },
            ],
            onyomi: [
                {
                    'kana': 'コウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['taka'],
        translation: 'haut, cher',
    },
    {
        kanji: '校',
        strokes: 10,
        level: 'N5',
        frequency: 294,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'コウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ko', 'kou'],
        translation: 'école, examen',
    },
    {
        kanji: '帰',
        strokes: 10,
        level: 'N4',
        frequency: 504,
        readings: {
            kunyomi: [
                {
                    'kana': 'かえ',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['kae'],
        translation: 'retourner',
    },
    {
        kanji: '魚',
        strokes: 11,
        level: 'N4',
        frequency: 1208,
        readings: {
            kunyomi: [
                {
                    'kana': 'さかな',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['sakana'],
        translation: 'poisson',
    },
    {
        kanji: '黒',
        strokes: 11,
        level: 'N4',
        frequency: 573,
        readings: {
            kunyomi: [
                {
                    'kana': 'くろ',
                },
            ],
            onyomi: [
                {
                    'kana': 'コク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kuro', 'koku'],
        translation: 'noir',
    },
    {
        kanji: '朝',
        strokes: 12,
        level: 'N4',
        frequency: 248,
        readings: {
            kunyomi: [
                {
                    'kana': 'あさ',
                },
            ],
            onyomi: [
                {
                    'kana': 'チョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['asa', 'cho', 'chou'],
        translation: 'matin, dynastie, époque',
    },
    {
        kanji: '買',
        strokes: 12,
        level: 'N4',
        frequency: 520,
        readings: {
            kunyomi: [
                {
                    'kana': 'か',
                },
            ],
            onyomi: [
                {
                    'kana': 'バイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ka', 'bai'],
        translation: 'achat',
    },
    {
        kanji: '新',
        strokes: 13,
        level: 'N4',
        frequency: 51,
        readings: {
            kunyomi: [
                {
                    'kana': 'あたら',
                },
            ],
            onyomi: [
                {
                    'kana': 'シン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['atara', 'shin'],
        translation: 'nouveauté',
    },
    {
        kanji: '楽',
        strokes: 13,
        level: 'N4',
        frequency: 373,
        readings: {
            kunyomi: [
                {
                    'kana': 'たの',
                },
            ],
            onyomi: [
                {
                    'kana': 'ガク',
                },
                {
                    'kana': 'ラク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['tano', 'gaku', 'raku'],
        translation: 'joie, confort',
    },
    {
        kanji: '電',
        strokes: 13,
        level: 'N5',
        frequency: 268,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'デン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['den'],
        translation: 'électricité',
    },
    {
        kanji: '駅',
        strokes: 14,
        level: 'N4',
        frequency: 724,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'エキ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['eki'],
        translation: 'gare, station',
    },
    {
        kanji: '薬',
        strokes: 16,
        level: 'N3',
        frequency: 702,
        readings: {
            kunyomi: [
                {
                    'kana': 'くすり',
                },
            ],
            onyomi: [
                {
                    'kana': 'ヤク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kusuri', 'yaku'],
        translation: 'médicament',
    },
    {
        kanji: '物',
        strokes: 8,
        level: 'N4',
        frequency: 215,
        readings: {
            kunyomi: [
                {
                    'kana': 'もの',
                },
            ],
            onyomi: [
                {
                    'kana': 'ブツ',
                },
                {
                    'kana': 'モツ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mono', 'butsu', 'motsu'],
        translation: 'objet',
    },
    {
        kanji: '会',
        strokes: 6,
        level: 'N4',
        frequency: 4,
        readings: {
            kunyomi: [
                {
                    'kana': 'あ',
                },
            ],
            onyomi: [
                {
                    'kana': 'カイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['a', 'kai'],
        translation: 'rencontre, réunion',
    },
    {
        kanji: '手',
        strokes: 4,
        level: 'N4',
        frequency: 60,
        readings: {
            kunyomi: [
                {
                    'kana': 'て',
                },
            ],
            onyomi: [
                {
                    'kana': 'シュ',
                },
                {
                    'kana': 'ズ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['te', 'shu', 'zu'],
        translation: 'main',
    },
    {
        kanji: '酒',
        strokes: 10,
        level: 'N3',
        frequency: 1006,
        readings: {
            kunyomi: [
                {
                    'kana': 'さけ',
                },
                {
                    'kana': 'さか',
                },
            ],
            onyomi: [
                {
                    'kana': 'シュ',
                },
            ],
        },
        romaji: ['sake', 'saka', 'shu'],
        translation: 'alcool, vin',
    },
    {
        kanji: '外',
        strokes: 5,
        level: 'N5',
        frequency: 81,
        readings: {
            kunyomi: [
                {
                    'kana': 'そと',
                },
            ],
            onyomi: [
                {
                    'kana': 'ガイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['soto', 'gai'],
        translation: 'extérieur',
    },
    {
        kanji: '愛',
        strokes: 13,
        level: 'N3',
        frequency: 640,
        readings: {
            kunyomi: [
                {
                    'kana': 'いと',
                },
                {
                    'kana': 'かな',
                },
                {
                    'kana': 'め',
                },
                {
                    'kana': 'まな',
                },
            ],
            onyomi: [
                {
                    'kana': 'アイ',
                },
            ],
        },
        romaji: ['ito', 'kana', 'me', 'mana', 'ai'],
        translation: 'amour, affection',
    },
    {
        kanji: '出',
        strokes: 5,
        level: 'N5',
        frequency: 13,
        readings: {
            kunyomi: [
                {
                    'kana': 'で',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['de'],
        translation: 'sortie',
    },
    {
        kanji: '半',
        strokes: 5,
        level: 'N5',
        frequency: 224,
        readings: {
            kunyomi: [
                {
                    'kana': 'なか',
                },
            ],
            onyomi: [
                {
                    'kana': 'ハン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['naka', 'han'],
        translation: 'demi',
    },
    {
        kanji: '冬',
        strokes: 5,
        level: 'N4',
        frequency: 1090,
        readings: {
            kunyomi: [
                {
                    'kana': 'ふゆ',
                },
            ],
            onyomi: [
                {
                    'kana': 'トウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['fuyu', 'to', 'tou'],
        translation: 'hiver',
    },
    {
        kanji: '右',
        strokes: 5,
        level: 'N5',
        frequency: 602,
        readings: {
            kunyomi: [
                {
                    'kana': 'みぎ',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['migi'],
        translation: 'droite',
    },
    {
        kanji: '左',
        strokes: 5,
        level: 'N5',
        frequency: 630,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひだり',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['hidari'],
        translation: 'gauche',
    },
    {
        kanji: '兄',
        strokes: 5,
        level: 'N4',
        frequency: 1219,
        readings: {
            kunyomi: [
                {
                    'kana': 'あに',
                },
            ],
            onyomi: [
                {
                    'kana': 'ケイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ani', 'kei'],
        translation: 'frère aîné',
    },
    {
        kanji: '名',
        strokes: 6,
        level: 'N5',
        frequency: 177,
        readings: {
            kunyomi: [
                {
                    'kana': 'な',
                },
            ],
            onyomi: [
                {
                    'kana': 'メイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['na', 'mei'],
        translation: 'nom',
    },
    {
        kanji: '弟',
        strokes: 7,
        level: 'N4',
        frequency: 1161,
        readings: {
            kunyomi: [
                {
                    'kana': 'おとうと',
                },
            ],
            onyomi: [
                {
                    'kana': 'テイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ototo', 'otouto', 'tei'],
        translation: 'frère cadet',
    },
    {
        kanji: '近',
        strokes: 7,
        level: 'N4',
        frequency: 194,
        readings: {
            kunyomi: [
                {
                    'kana': 'ちか',
                },
            ],
            onyomi: [
                {
                    'kana': 'キン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['chika', 'kin'],
        translation: 'proche',
    },
    {
        kanji: '長',
        strokes: 8,
        level: 'N5',
        frequency: 12,
        readings: {
            kunyomi: [
                {
                    'kana': 'なが',
                },
            ],
            onyomi: [
                {
                    'kana': 'チョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['naga', 'cho', 'chou'],
        translation: 'long, chef',
    },
    {
        kanji: '夜',
        strokes: 8,
        level: 'N4',
        frequency: 487,
        readings: {
            kunyomi: [
                {
                    'kana': 'よる',
                },
                {
                    'kana': 'よ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ヤ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['yoru', 'yo', 'ya'],
        translation: 'nuit',
    },
    {
        kanji: '姉',
        strokes: 8,
        level: 'N4',
        frequency: 1473,
        readings: {
            kunyomi: [
                {
                    'kana': 'あね',
                },
            ],
            onyomi: [
                {
                    'kana': 'シ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ane', 'shi'],
        translation: 'sœur aînée',
    },
    {
        kanji: '妹',
        strokes: 8,
        level: 'N4',
        frequency: 1446,
        readings: {
            kunyomi: [
                {
                    'kana': 'いもうと',
                },
            ],
            onyomi: [
                {
                    'kana': 'マイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['imoto', 'imouto', 'mai'],
        translation: 'sœur cadette',
    },
    {
        kanji: '彼',
        strokes: 8,
        level: 'N3',
        frequency: 648,
        readings: {
            kunyomi: [
                {
                    'kana': 'かれ',
                },
                {
                    'kana': 'かの',
                },
            ],
            onyomi: [],
        },
        collections: [
            1,
        ],
        romaji: ['kare', 'kano'],
        translation: 'il',
    },
    {
        kanji: '青',
        strokes: 8,
        level: 'N4',
        frequency: 589,
        readings: {
            kunyomi: [
                {
                    'kana': 'あお',
                },
            ],
            onyomi: [
                {
                    'kana': 'セイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ao', 'sei'],
        translation: 'bleu',
    },
    {
        kanji: '春',
        strokes: 9,
        level: 'N4',
        frequency: 579,
        readings: {
            kunyomi: [
                {
                    'kana': 'はる',
                },
            ],
            onyomi: [
                {
                    'kana': 'シュン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['haru', 'shun'],
        translation: 'printemps',
    },
    {
        kanji: '秋',
        strokes: 9,
        level: 'N4',
        frequency: 635,
        readings: {
            kunyomi: [
                {
                    'kana': 'あき',
                },
            ],
            onyomi: [
                {
                    'kana': 'シュウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['aki', 'shu', 'shuu'],
        translation: 'automne',
    },
    {
        kanji: '前',
        strokes: 9,
        level: 'N5',
        frequency: 27,
        readings: {
            kunyomi: [
                {
                    'kana': 'まえ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゼン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mae', 'zen'],
        translation: 'devant, avant',
    },
    {
        kanji: '後',
        strokes: 9,
        level: 'N5',
        frequency: 26,
        readings: {
            kunyomi: [
                {
                    'kana': 'あと',
                },
                {
                    'kana': 'うし',
                },
                {
                    'kana': 'のち',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゴ',
                },
                {
                    'kana': 'コウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ato', 'ushi', 'nochi', 'go', 'ko', 'kou'],
        translation: 'derrière, après',
    },
    {
        kanji: '夏',
        strokes: 10,
        level: 'N4',
        frequency: 659,
        readings: {
            kunyomi: [
                {
                    'kana': 'なつ',
                },
            ],
            onyomi: [
                {
                    'kana': 'カ',
                },
                {
                    'kana': 'ゲ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['natsu', 'ka', 'ge'],
        translation: 'été',
    },
    {
        kanji: '家',
        strokes: 10,
        level: 'N4',
        frequency: 133,
        readings: {
            kunyomi: [
                {
                    'kana': 'いえ',
                },
                {
                    'kana': 'や',
                },
            ],
            onyomi: [
                {
                    'kana': 'カ',
                },
                {
                    'kana': 'ケ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ie', 'ya', 'ka', 'ke'],
        translation: 'maison, foyer',
    },
    {
        kanji: '週',
        strokes: 11,
        level: 'N4',
        frequency: 540,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'シュウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['shu', 'shuu'],
        translation: 'semaine',
    },
    {
        kanji: '閉',
        strokes: 11,
        level: 'N3',
        frequency: 951,
        readings: {
            kunyomi: [
                {
                    'kana': 'し',
                },
                {
                    'kana': 'と',
                },
            ],
            onyomi: [
                {
                    'kana': 'ヘイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['shi', 'to', 'hei'],
        translation: 'fermeture',
    },
    {
        kanji: '晩',
        strokes: 12,
        level: 'N3',
        frequency: 1424,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'バン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ban'],
        translation: 'soir',
    },
    {
        kanji: '晴',
        strokes: 12,
        level: 'N3',
        frequency: 1022,
        readings: {
            kunyomi: [
                {
                    'kana': 'は',
                },
                {
                    'kana': 'ば',
                },
            ],
            onyomi: [
                {
                    'kana': 'セイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ha', 'ba', 'sei'],
        translation: 's\'éclaircir',
    },
    {
        kanji: '森',
        strokes: 12,
        level: 'N2',
        frequency: 609,
        readings: {
            kunyomi: [
                {
                    'kana': 'もり',
                },
            ],
            onyomi: [
                {
                    'kana': 'シン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['mori', 'shin'],
        translation: 'forêt',
    },
    {
        kanji: '開',
        strokes: 12,
        level: 'N4',
        frequency: 59,
        readings: {
            kunyomi: [
                {
                    'kana': 'あ',
                },
                {
                    'kana': 'ひら',
                },
            ],
            onyomi: [
                {
                    'kana': 'カイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['a', 'hira', 'kai'],
        translation: 'ouverture',
    },
    {
        kanji: '間',
        strokes: 12,
        level: 'N5',
        frequency: 33,
        readings: {
            kunyomi: [
                {
                    'kana': 'あいだ',
                },
                {
                    'kana': 'ま',
                },
            ],
            onyomi: [
                {
                    'kana': 'カナ',
                },
                {
                    'kana': 'ケン',
                },
                {
                    'kana': 'ゲン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['aida', 'ma', 'kana', 'ken', 'gen'],
        translation: 'intervalle',
    },
    {
        kanji: '明',
        strokes: 8,
        level: 'N4',
        frequency: 67,
        readings: {
            kunyomi: [
                {
                    'kana': 'あか',
                },
                {
                    'kana': 'あ',
                },
            ],
            onyomi: [
                {
                    'kana': 'メイ',
                },
                {
                    'kana': 'ミョウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['aka', 'a', 'mei', 'myo', 'myou'],
        translation: 'clarté',
    },
    {
        kanji: '暗',
        strokes: 13,
        level: 'N3',
        frequency: 1040,
        readings: {
            kunyomi: [
                {
                    'kana': 'くら',
                },
                {
                    'kana': 'ぐら',
                },
            ],
            onyomi: [
                {
                    'kana': 'アン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kura', 'gura', 'an'],
        translation: 'obscurité',
    },
    {
        kanji: '毎',
        strokes: 6,
        level: 'N5',
        frequency: 436,
        readings: {
            kunyomi: [
                {
                    'kana': 'ごと',
                },
            ],
            onyomi: [
                {
                    'kana': 'マイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['goto', 'mai'],
        translation: 'chaque',
    },
    {
        kanji: '訓',
        strokes: 10,
        level: 'N2',
        frequency: 1134,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'クン',
                },
            ],
        },
        romaji: ['kun'],
        translation: 'instruction, lecture japonaise',
    },
    {
        kanji: '飯',
        strokes: 12,
        level: 'N4',
        frequency: 1046,
        readings: {
            kunyomi: [
                {
                    'kana': 'めし',
                },
            ],
            onyomi: [
                {
                    'kana': 'ハン',
                },
            ],
        },
        romaji: ['meshi', 'han'],
        translation: 'repas, riz cuit',
    },
    {
        kanji: '茶',
        strokes: 9,
        level: 'N4',
        frequency: 1116,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'チャ',
                },
            ],
        },
        romaji: ['cha'],
        translation: 'thé',
    },
    {
        kanji: '肉',
        strokes: 6,
        level: 'N4',
        frequency: 986,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'ニク',
                },
            ],
        },
        romaji: ['niku'],
        translation: 'viande',
    },
    {
        kanji: '果',
        strokes: 8,
        level: 'N3',
        frequency: 258,
        readings: {
            kunyomi: [
                {
                    'kana': 'は',
                },
            ],
            onyomi: [
                {
                    'kana': 'カ',
                },
                {
                    'kana': 'クダ',
                },
            ],
        },
        romaji: ['ha', 'ka', 'kuda'],
        translation: 'accomplissement, récompense, succès',
    },
    {
        kanji: '菓',
        strokes: 11,
        level: 'N2',
        frequency: 1719,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'カ',
                },
            ],
        },
        romaji: ['ka'],
        translation: 'confiserie, gâteau',
    },
    {
        kanji: '汁',
        strokes: 5,
        level: 'N1',
        frequency: 1843,
        readings: {
            kunyomi: [
                {
                    'kana': 'しる',
                },
            ],
            onyomi: [],
        },
        romaji: ['shiru'],
        translation: 'jus, liquide',
    },
    {
        kanji: '洋',
        strokes: 9,
        level: 'N4',
        frequency: 763,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'ヨウ',
                },
            ],
        },
        romaji: ['yo', 'you'],
        translation: 'océan, étranger',
    },
    {
        kanji: '島',
        strokes: 10,
        level: 'N2',
        frequency: 245,
        readings: {
            kunyomi: [
                {
                    'kana': 'しま',
                },
            ],
            onyomi: [
                {
                    'kana': 'トウ',
                },
            ],
        },
        romaji: ['shima', 'to', 'tou'],
        translation: 'île',
    },
    {
        kanji: '松',
        strokes: 8,
        level: 'N1',
        frequency: 471,
        readings: {
            kunyomi: [
                {
                    'kana': 'まつ',
                },
            ],
            onyomi: [],
        },
        romaji: ['matsu'],
        translation: 'pin',
    },
    {
        kanji: '宮',
        strokes: 10,
        level: 'N1',
        frequency: 367,
        readings: {
            kunyomi: [
                {
                    'kana': 'みや',
                },
            ],
            onyomi: [
                {
                    'kana': 'キュウ',
                },
                {
                    'kana': 'グウ',
                },
                {
                    'kana': 'ク',
                },
                {
                    'kana': 'クウ',
                },
            ],
        },
        romaji: ['miya', 'kyu', 'kyuu', 'gu', 'guu', 'ku', 'kuu'],
        translation: 'sanctuaire shintoïste, palais',
    },
    {
        kanji: '崎',
        strokes: 11,
        level: 'N1',
        frequency: 533,
        readings: {
            kunyomi: [
                {
                    'kana': 'さき',
                },
                {
                    'kana': 'ざき',
                },
            ],
            onyomi: [],
        },
        romaji: ['saki', 'zaki'],
        translation: 'prémontoire',
    },
    {
        kanji: '広',
        strokes: 5,
        level: 'N4',
        frequency: 263,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひろ',
                },
            ],
            onyomi: [],
        },
        romaji: ['hiro'],
        translation: 'large, spacieux',
    },
    {
        kanji: '阪',
        strokes: 7,
        level: 'N3',
        frequency: 503,
        readings: {
            kunyomi: [
                {
                    'kana': 'さか',
                },
            ],
            onyomi: [
                {
                    'kana': 'ハン',
                },
            ],
        },
        romaji: ['saka', 'han'],
        translation: 'hauteur (terrain)',
    },
    {
        kanji: '人',
        strokes: 2,
        level: 'N5',
        frequency: 5,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひと',
                },
                {
                    'kana': 'り',
                },
                {
                    'kana': 'と',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジン',
                },
                {
                    'kana': 'ニン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['hito', 'ri', 'to', 'jin', 'nin'],
        translation: 'personne',
    },
    {
        kanji: '午',
        strokes: 4,
        level: 'N5',
        frequency: 154,
        readings: {
            kunyomi: [
                {
                    'kana': 'うま',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゴ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['uma', 'go'],
        translation: 'midi',
    },
    {
        kanji: '友',
        strokes: 4,
        level: 'N5',
        frequency: 622,
        readings: {
            kunyomi: [
                {
                    'kana': 'とも',
                },
            ],
            onyomi: [
                {
                    'kana': 'ユウ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['tomo', 'yu', 'yuu'],
        translation: 'ami',
    },
    {
        kanji: '英',
        strokes: 8,
        level: 'N4',
        frequency: 430,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'エイ',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ei'],
        translation: 'excellent, anglais',
    },
    {
        kanji: '漢',
        strokes: 13,
        level: 'N4',
        frequency: 1487,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'カン',
                },
            ],
        },
        romaji: ['kan'],
        translation: 'Chine',
    },
    {
        kanji: '遊',
        strokes: 12,
        level: 'N3',
        frequency: 941,
        readings: {
            kunyomi: [
                {
                    'kana': 'あそ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ユウ',
                },
            ],
        },
        romaji: ['aso', 'yu', 'yuu'],
        translation: 'jeu, jouer',
    },
    {
        kanji: '寝',
        strokes: 13,
        level: 'N3',
        frequency: 1034,
        readings: {
            kunyomi: [
                {
                    'kana': 'ね',
                },
            ],
            onyomi: [
                {
                    'kana': 'シン',
                },
            ],
        },
        romaji: ['ne', 'shin'],
        translation: 'sommeil, être allongé, dormir',
    },
    {
        kanji: '乗',
        strokes: 9,
        level: 'N3',
        frequency: 377,
        readings: {
            kunyomi: [
                {
                    'kana': 'の',
                },
            ],
            onyomi: [
                {
                    'kana': 'ショウ',
                },
                {
                    'kana': 'ジョウ',
                },
            ],
        },
        romaji: ['no', 'sho', 'shou', 'jo', 'jou'],
        translation: 'à bord, puissance, multiplication',
    },
    {
        kanji: '待',
        strokes: 9,
        level: 'N4',
        frequency: 391,
        readings: {
            kunyomi: [
                {
                    'kana': 'ま',
                },
            ],
            onyomi: [
                {
                    'kana': 'タイ',
                },
            ],
        },
        romaji: ['ma', 'tai'],
        translation: 'attente',
    },
    {
        kanji: '立',
        strokes: 5,
        level: 'N4',
        frequency: 58,
        readings: {
            kunyomi: [
                {
                    'kana': 'た',
                },
            ],
            onyomi: [
                {
                    'kana': 'リツ',
                },
                {
                    'kana': 'リュウ',
                },
                {
                    'kana': 'リットル',
                },
            ],
        },
        romaji: ['ta', 'ritsu', 'ryu', 'ryuu', 'rittoru'],
        translation: 'debout, s\'ériger',
    },
    {
        kanji: '急',
        strokes: 9,
        level: 'N4',
        frequency: 309,
        readings: {
            kunyomi: [
                {
                    'kana': 'いそ',
                },
            ],
            onyomi: [
                {
                    'kana': 'キュウ',
                },
            ],
        },
        romaji: ['iso', 'kyu', 'kyuu'],
        translation: 'urgence, soudain',
    },
    {
        kanji: '死',
        strokes: 6,
        level: 'N4',
        frequency: 229,
        readings: {
            kunyomi: [
                {
                    'kana': 'し',
                },
            ],
            onyomi: [
                {
                    'kana': 'シ',
                },
            ],
        },
        romaji: ['shi'],
        translation: 'mort',
    },
    {
        kanji: '売',
        strokes: 7,
        level: 'N4',
        frequency: 202,
        readings: {
            kunyomi: [
                {
                    'kana': 'う',
                },
            ],
            onyomi: [
                {
                    'kana': 'バイ',
                },
            ],
        },
        romaji: ['u', 'bai'],
        translation: 'vente',
    },
    {
        kanji: '探',
        strokes: 11,
        level: 'N3',
        frequency: 930,
        readings: {
            kunyomi: [
                {
                    'kana': 'さが',
                },
                {
                    'kana': 'さぐ',
                },
            ],
            onyomi: [
                {
                    'kana': 'タン',
                },
            ],
        },
        romaji: ['saga', 'sagu', 'tan'],
        translation: 'recherche',
    },
    {
        kanji: '勝',
        strokes: 12,
        level: 'N3',
        frequency: 185,
        readings: {
            kunyomi: [
                {
                    'kana': 'か',
                },
                {
                    'kana': 'まさ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ショウ',
                },
            ],
        },
        romaji: ['ka', 'masa', 'sho', 'shou'],
        translation: 'victoire, excellence',
    },
    {
        kanji: '泳',
        strokes: 8,
        level: 'N3',
        frequency: 1223,
        readings: {
            kunyomi: [
                {
                    'kana': 'およ',
                },
            ],
            onyomi: [
                {
                    'kana': 'エイ',
                },
            ],
        },
        romaji: ['oyo', 'ei'],
        translation: 'nage',
    },
    {
        kanji: '才',
        strokes: 3,
        level: 'N3',
        frequency: 1497,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'サイ',
                },
            ],
        },
        romaji: ['sai'],
        translation: 'génie',
    },
    {
        kanji: '心',
        strokes: 4,
        level: 'N4',
        frequency: 157,
        readings: {
            kunyomi: [
                {
                    'kana': 'こころ',
                },
            ],
            onyomi: [
                {
                    'kana': 'シン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kokoro', 'shin'],
        translation: 'cœur, esprit',
    },
    {
        kanji: '足',
        strokes: 7,
        level: 'N4',
        frequency: 343,
        readings: {
            kunyomi: [
                {
                    'kana': 'あし',
                },
                {
                    'kana': 'た',
                },
            ],
            onyomi: [
                {
                    'kana': 'ソク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ashi', 'ta', 'soku'],
        translation: 'pied, suffire',
    },
    {
        kanji: '屋',
        strokes: 9,
        level: 'N4',
        frequency: 616,
        readings: {
            kunyomi: [
                {
                    'kana': 'や',
                },
            ],
            onyomi: [
                {
                    'kana': 'オク',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['ya', 'oku'],
        translation: 'maison, marchand',
    },
    {
        kanji: '門',
        strokes: 8,
        level: 'N2',
        frequency: 452,
        readings: {
            kunyomi: [
                {
                    'kana': 'かど',
                },
            ],
            onyomi: [
                {
                    'kana': 'モン',
                },
            ],
        },
        collections: [
            1,
        ],
        romaji: ['kado', 'mon'],
        translation: 'porte',
    },
    {
        kanji: '願',
        strokes: 19,
        level: 'N3',
        frequency: 894,
        readings: {
            kunyomi: [
                {
                    'kana': 'ねが',
                },
            ],
            onyomi: [
                {
                    'kana': 'ガン',
                },
            ],
        },
        romaji: ['nega', 'gan'],
        translation: 'requête, vœu',
    },
    {
        kanji: '宜',
        strokes: 8,
        level: 'N1',
        frequency: 1766,
        readings: {
            kunyomi: [
                {
                    'kana': 'よろ',
                },
            ],
            onyomi: [],
        },
        romaji: ['yoro'],
        translation: 'correctitude',
    },
    {
        kanji: '初',
        strokes: 7,
        level: 'N3',
        frequency: 152,
        readings: {
            kunyomi: [
                {
                    'kana': 'はじ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ショ',
                },
            ],
        },
        romaji: ['haji', 'sho'],
        translation: 'première fois, début',
    },
    {
        kanji: '勉',
        strokes: 10,
        level: 'N4',
        frequency: 1066,
        readings: {
            kunyomi: [
                {
                    'kana': 'つと',
                },
            ],
            onyomi: [
                {
                    'kana': 'ベン',
                },
            ],
        },
        romaji: ['tsuto', 'ben'],
        translation: 'effort',
    },
    {
        kanji: '強',
        strokes: 11,
        level: 'N4',
        frequency: 112,
        readings: {
            kunyomi: [
                {
                    'kana': 'つよ',
                },
            ],
            onyomi: [
                {
                    'kana': 'キョウ',
                },
            ],
        },
        romaji: ['tsuyo', 'kyo', 'kyou'],
        translation: 'force',
    },
    {
        kanji: '寺',
        strokes: 6,
        level: 'N2',
        frequency: 879,
        readings: {
            kunyomi: [
                {
                    'kana': 'てら',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジ',
                },
            ],
        },
        romaji: ['tera', 'ji'],
        translation: 'temple boudhiste',
    },
    {
        kanji: '神',
        strokes: 9,
        level: 'N3',
        frequency: 347,
        readings: {
            kunyomi: [
                {
                    'kana': 'かみ',
                },
            ],
            onyomi: [
                {
                    'kana': 'シン',
                },
                {
                    'kana': 'ジン',
                },
            ],
        },
        romaji: ['kami', 'shin', 'jin'],
        translation: 'dieux, divin',
    },
    {
        kanji: '殿',
        strokes: 13,
        level: 'N2',
        frequency: 1199,
        readings: {
            kunyomi: [
                {
                    'kana': 'との',
                },
            ],
            onyomi: [
                {
                    'kana': 'デン',
                },
                {
                    'kana': 'テン',
                },
            ],
        },
        romaji: ['tono', 'den', 'ten'],
        translation: 'seigneur, palais, temple',
    },
    {
        kanji: '社',
        strokes: 7,
        level: 'N4',
        frequency: 21,
        readings: {
            kunyomi: [
                {
                    'kana': 'やしろ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジャ',
                },
            ],
        },
        romaji: ['yashiro', 'ja'],
        translation: 'compagnie, bureau, sanctuaire',
    },
    {
        kanji: '歳',
        strokes: 13,
        level: 'N3',
        frequency: 269,
        readings: {
            kunyomi: [
                {
                    'kana': 'とし',
                },
                {
                    'kana': 'とせ',
                },
                {
                    'kana': 'よわい',
                },
            ],
            onyomi: [
                {
                    'kana': 'サイ',
                },
                {
                    'kana': 'セイ',
                },
            ],
        },
        romaji: ['toshi', 'tose', 'yowai', 'sai', 'sei'],
        translation: 'âge, fin d\'année, occasion',
    },
    {
        kanji: '忙',
        strokes: 6,
        level: 'N3',
        frequency: 1475,
        readings: {
            kunyomi: [
                {
                    'kana': 'いそが',
                },
                {
                    'kana': 'せわ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ボウ',
                },
                {
                    'kana': 'モウ',
                },
            ],
        },
        romaji: ['isoga', 'sewa', 'bo', 'bou', 'mo', 'mou'],
        translation: 'occupé, affairé, agité',
    },
    {
        kanji: '映',
        strokes: 9,
        level: 'N4',
        frequency: 404,
        readings: {
            kunyomi: [
                {
                    'kana': 'うつ',
                },
                {
                    'kana': 'は',
                },
                {
                    'kana': 'ば',
                },
            ],
            onyomi: [
                {
                    'kana': 'エイ',
                },
            ],
        },
        romaji: ['utsu', 'ha', 'ba', 'ei'],
        translation: 'reflet, projection',
    },
    {
        kanji: '画',
        strokes: 8,
        level: 'N4',
        frequency: 199,
        readings: {
            kunyomi: [
                {
                    'kana': 'かく',
                },
            ],
            onyomi: [
                {
                    'kana': 'エ',
                },
                {
                    'kana': 'ガ',
                },
            ],
        },
        romaji: ['kaku', 'e', 'ga'],
        translation: 'trait de pinceau, dessin',
    },
    {
        kanji: '館',
        strokes: 16,
        level: 'N4',
        frequency: 613,
        readings: {
            kunyomi: [
                {
                    'kana': 'やかた',
                },
            ],
            onyomi: [
                {
                    'kana': 'カン',
                },
            ],
        },
        romaji: ['yakata', 'kan'],
        translation: 'bâtiment, résidence, palais',
    },
    {
        kanji: '元',
        strokes: 4,
        level: 'N4',
        frequency: 192,
        readings: {
            kunyomi: [
                {
                    'kana': 'もと',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゲン',
                },
                {
                    'kana': 'ガン',
                },
            ],
        },
        romaji: ['moto', 'gen', 'gan'],
        translation: 'début, origine',
    },
    {
        kanji: '最',
        strokes: 12,
        level: 'N3',
        frequency: 82,
        readings: {
            kunyomi: [
                {
                    'kana': 'もっと',
                },
            ],
            onyomi: [
                {
                    'kana': 'サイ',
                },
            ],
        },
        romaji: ['motto', 'sai'],
        translation: 'maximum, extrême',
    },
    {
        kanji: '当',
        strokes: 6,
        level: 'N3',
        frequency: 91,
        readings: {
            kunyomi: [
                {
                    'kana': 'あ',
                },
            ],
            onyomi: [
                {
                    'kana': 'トウ',
                },
            ],
        },
        romaji: ['a', 'to', 'tou'],
        translation: 'juste, dans le mille',
    },
    {
        kanji: '変',
        strokes: 9,
        level: 'N3',
        frequency: 238,
        readings: {
            kunyomi: [
                {
                    'kana': 'か',
                },
            ],
            onyomi: [
                {
                    'kana': 'ヘン',
                },
            ],
        },
        romaji: ['ka', 'hen'],
        translation: 'inusuel, étrange',
    },
    {
        kanji: '丈',
        strokes: 3,
        level: null,
        frequency: 1375,
        readings: {
            kunyomi: [
                {
                    'kana': 'たけ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジョウ',
                },
            ],
        },
        romaji: ['take', 'jo', 'jou'],
        translation: 'longueur, ten shaku (mesure)',
    },
    {
        kanji: '夫',
        strokes: 4,
        level: 'N3',
        frequency: 335,
        readings: {
            kunyomi: [
                {
                    'kana': 'おっと',
                },
            ],
            onyomi: [
                {
                    'kana': 'フ',
                },
                {
                    'kana': 'フウ',
                },
                {
                    'kana': 'ブ',
                },
            ],
        },
        romaji: ['otto', 'fu', 'fuu', 'bu'],
        translation: 'mari, homme',
    },
    {
        kanji: '程',
        strokes: 12,
        level: 'N3',
        frequency: 514,
        readings: {
            kunyomi: [
                {
                    'kana': 'ほど',
                },
            ],
            onyomi: [
                {
                    'kana': 'テイ',
                },
            ],
        },
        romaji: ['hodo', 'tei'],
        translation: 'ampleur, degré',
    },
    {
        kanji: '皆',
        strokes: 9,
        level: 'N3',
        frequency: 1267,
        readings: {
            kunyomi: [
                {
                    'kana': 'みな',
                },
                {
                    'kana': 'みんな',
                },
            ],
            onyomi: [
                {
                    'kana': 'カイ',
                },
            ],
        },
        romaji: ['mina', 'minna', 'kai'],
        translation: 'tout',
    },
    {
        kanji: '暑',
        strokes: 12,
        level: 'N1',
        frequency: 1442,
        readings: {
            kunyomi: [
                {
                    'kana': 'あつ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ショ',
                },
            ],
        },
        romaji: ['atsu', 'sho'],
        translation: 'chaleur, chaleur d\'été',
    },
    {
        kanji: '熱',
        strokes: 15,
        level: 'N3',
        frequency: 700,
        readings: {
            kunyomi: [
                {
                    'kana': 'あつ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ネツ',
                },
            ],
        },
        romaji: ['atsu', 'netsu'],
        translation: 'chaleur, fièvre, fièvre',
    },
    {
        kanji: '寒',
        strokes: 12,
        level: 'N3',
        frequency: 1456,
        readings: {
            kunyomi: [
                {
                    'kana': 'さむ',
                },
            ],
            onyomi: [
                {
                    'kana': 'カン',
                },
            ],
        },
        romaji: ['samu', 'kan'],
        translation: 'froid',
    },
    {
        kanji: '迚',
        strokes: 8,
        level: null,
        frequency: 2500,
        readings: {
            kunyomi: [
                {
                    'kana': 'とて',
                },
            ],
            onyomi: [],
        },
        romaji: ['tote'],
        translation: 'd\'une façon ou d\'une autre, très',
    },
    {
        kanji: '若',
        strokes: 8,
        level: 'N3',
        frequency: 458,
        readings: {
            kunyomi: [
                {
                    'kana': 'わか',
                },
                {
                    'kana': 'も',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジャク',
                },
                {
                    'kana': 'ニャク',
                },
            ],
        },
        romaji: ['waka', 'mo', 'jaku', 'nyaku'],
        translation: 'jeunesse, immaturité, possibilité',
    },
    {
        kanji: '幾',
        strokes: 12,
        level: 'N3',
        frequency: 1725,
        readings: {
            kunyomi: [
                {
                    'kana': 'いく',
                },
            ],
            onyomi: [
                {
                    'kana': 'キ',
                },
            ],
        },
        romaji: ['iku', 'ki'],
        translation: 'combien',
    },
    {
        kanji: '難',
        strokes: 18,
        level: 'N3',
        frequency: 330,
        readings: {
            kunyomi: [
                {
                    'kana': 'かた',
                },
                {
                    'kana': 'むずか',
                },
            ],
            onyomi: [
                {
                    'kana': 'ナン',
                },
            ],
        },
        romaji: ['kata', 'muzuka', 'nan'],
        translation: 'difficulté',
    },
    {
        kanji: '簡',
        strokes: 18,
        level: 'N2',
        frequency: 983,
        readings: {
            kunyomi: [
                {
                    'kana': 'ふだ',
                },
            ],
            onyomi: [
                {
                    'kana': 'カン',
                },
                {
                    'kana': 'ケン',
                },
            ],
        },
        romaji: ['fuda', 'kan', 'ken'],
        translation: 'simplicité, brièveté',
    },
    {
        kanji: '単',
        strokes: 9,
        level: 'N3',
        frequency: 586,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひとえ',
                },
            ],
            onyomi: [
                {
                    'kana': 'タン',
                },
            ],
        },
        romaji: ['hitoe', 'tan'],
        translation: 'un, unique, seulement',
    },
    {
        kanji: '多',
        strokes: 6,
        level: 'N4',
        frequency: 139,
        readings: {
            kunyomi: [
                {
                    'kana': 'おお',
                },
            ],
            onyomi: [
                {
                    'kana': 'タ',
                },
            ],
        },
        romaji: ['oo', 'ta'],
        translation: 'beaucoup, fréquent',
    },
    {
        kanji: '誰',
        strokes: 15,
        level: null,
        frequency: 1933,
        readings: {
            kunyomi: [
                {
                    'kana': 'だれ',
                },
            ],
            onyomi: [
                {
                    'kana': 'スイ',
                },
            ],
        },
        romaji: ['dare', 'sui'],
        translation: 'qui, quelqu\'un',
    },
    {
        kanji: '面',
        strokes: 9,
        level: 'N3',
        frequency: 186,
        readings: {
            kunyomi: [
                {
                    'kana': 'おも',
                },
                {
                    'kana': 'おもて',
                },
                {
                    'kana': 'つら',
                },
            ],
            onyomi: [
                {
                    'kana': 'メン',
                },
                {
                    'kana': 'ベン',
                },
            ],
        },
        romaji: ['omo', 'omote', 'tsura', 'men', 'ben'],
        translation: 'masque, visage, traits',
    },
    {
        kanji: '番',
        strokes: 12,
        level: 'N3',
        frequency: 348,
        readings: {
            kunyomi: [
                {
                    'kana': 'つが',
                },
            ],
            onyomi: [
                {
                    'kana': 'バン',
                },
            ],
        },
        romaji: ['tsuga', 'ban'],
        translation: 'tour, numéro, ordre',
    },
    {
        kanji: '余',
        strokes: 7,
        level: 'N3',
        frequency: 680,
        readings: {
            kunyomi: [
                {
                    'kana': 'あま',
                },
            ],
            onyomi: [
                {
                    'kana': 'ヨ',
                },
            ],
        },
        romaji: ['ama', 'yo'],
        translation: 'excès',
    },
    {
        kanji: '事',
        strokes: 8,
        level: 'N4',
        frequency: 18,
        readings: {
            kunyomi: [
                {
                    'kana': 'こと',
                },
                {
                    'kana': 'つか',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジ',
                },
                {
                    'kana': 'ズ',
                },
            ],
        },
        romaji: ['koto', 'tsuka', 'ji', 'zu'],
        translation: 'fait, chose, matière',
    },
    {
        kanji: '全',
        strokes: 6,
        level: 'N3',
        frequency: 75,
        readings: {
            kunyomi: [
                {
                    'kana': 'まった',
                },
                {
                    'kana': 'すべ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゼン',
                },
            ],
        },
        romaji: ['matta', 'sube', 'zen'],
        translation: 'entier, tout, complet',
    },
    {
        kanji: '然',
        strokes: 12,
        level: 'N3',
        frequency: 401,
        readings: {
            kunyomi: [
                {
                    'kana': 'しか',
                },
                {
                    'kana': 'さ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゼン',
                },
                {
                    'kana': 'ネン',
                },
            ],
        },
        romaji: ['shika', 'sa', 'zen', 'nen'],
        translation: 'état naturel, suffixe adverbial',
    },
    {
        kanji: '趣',
        strokes: 15,
        level: 'N1',
        frequency: 1153,
        readings: {
            kunyomi: [
                {
                    'kana': 'おもむき',
                },
            ],
            onyomi: [
                {
                    'kana': 'シュ',
                },
            ],
        },
        romaji: ['omomuki', 'shu'],
        translation: 'teneur, apparence',
    },
    {
        kanji: '味',
        strokes: 8,
        level: 'N4',
        frequency: 442,
        readings: {
            kunyomi: [
                {
                    'kana': 'あじ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ミ',
                },
            ],
        },
        romaji: ['aji', 'mi'],
        translation: 'saveur',
    },
    {
        kanji: '切',
        strokes: 4,
        level: 'N4',
        frequency: 324,
        readings: {
            kunyomi: [
                {
                    'kana': 'き',
                },
                {
                    'kana': 'ぎ',
                },
            ],
            onyomi: [
                {
                    'kana': 'セツ',
                },
                {
                    'kana': 'サイ',
                },
            ],
        },
        romaji: ['ki', 'gi', 'setsu', 'sai'],
        translation: 'coupant, tranchant',
    },
    {
        kanji: '部',
        strokes: 11,
        level: 'N3',
        frequency: 36,
        readings: {
            kunyomi: [
                {
                    'kana': 'へ',
                },
                {
                    'kana': 'べ',
                },
            ],
            onyomi: [
                {
                    'kana': 'フ',
                },
                {
                    'kana': 'ブ',
                },
                {
                    'kana': 'ホ',
                },
            ],
        },
        romaji: ['he', 'be', 'fu', 'bu', 'ho'],
        translation: 'section, bureau, departement',
    },
    {
        kanji: '静',
        strokes: 14,
        level: 'N3',
        frequency: 764,
        readings: {
            kunyomi: [
                {
                    'kana': 'しず',
                },
            ],
            onyomi: [
                {
                    'kana': 'セイ',
                },
                {
                    'kana': 'ジョウ',
                },
            ],
        },
        romaji: ['shizu', 'sei', 'jo', 'jou'],
        translation: 'calme',
    },
    {
        kanji: '優',
        strokes: 17,
        level: 'N3',
        frequency: 334,
        readings: {
            kunyomi: [
                {
                    'kana': 'やさ',
                },
                {
                    'kana': 'すぐ',
                },
                {
                    'kana': 'まさ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ユウ',
                },
                {
                    'kana': 'ウ',
                },
            ],
        },
        romaji: ['yasa', 'sugu', 'masa', 'yu', 'yuu', 'u'],
        translation: 'tendresse, gentillesse, supériorité',
    },
    {
        kanji: '必',
        strokes: 5,
        level: 'N3',
        frequency: 265,
        readings: {
            kunyomi: [
                {
                    'kana': 'かなら',
                },
            ],
            onyomi: [
                {
                    'kana': 'ヒツ',
                },
            ],
        },
        romaji: ['kanara', 'hitsu'],
        translation: 'nécessairement, certain, inévitable',
    },
    {
        kanji: '要',
        strokes: 9,
        level: 'N3',
        frequency: 106,
        readings: {
            kunyomi: [
                {
                    'kana': 'い',
                },
                {
                    'kana': 'かなめ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ヨウ',
                },
            ],
        },
        romaji: ['i', 'kaname', 'yo', 'you'],
        translation: 'besoin, point principal, essence',
    },
    {
        kanji: '勿',
        strokes: 4,
        level: null,
        frequency: 2500,
        readings: {
            kunyomi: [
                {
                    'kana': 'はか',
                },
                {
                    'kana': 'なし',
                },
            ],
            onyomi: [
                {
                    'kana': 'モチ',
                },
                {
                    'kana': 'ブツ',
                },
                {
                    'kana': 'ボツ',
                },
            ],
        },
        romaji: ['haka', 'nashi', 'mochi', 'butsu', 'botsu'],
        translation: 'ne pas, négation',
    },
    {
        kanji: '論',
        strokes: 15,
        level: 'N3',
        frequency: 227,
        readings: {
            kunyomi: [
                {
                    'kana': 'あげつら',
                },
            ],
            onyomi: [
                {
                    'kana': 'ロン',
                },
            ],
        },
        romaji: ['agetsura', 'ron'],
        translation: 'argumentation, discours',
    },
    {
        kanji: '蜂',
        strokes: 13,
        level: null,
        frequency: 2223,
        readings: {
            kunyomi: [
                {
                    'kana': 'はち',
                },
            ],
            onyomi: [
                {
                    'kana': 'ホウ',
                },
            ],
        },
        romaji: ['hachi', 'ho', 'hou'],
        translation: 'abeille, guêpe',
    },
    {
        kanji: '員',
        strokes: 10,
        level: 'N4',
        frequency: 54,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'イン',
                },
            ],
        },
        romaji: ['in'],
        translation: 'employé, membre, chargé',
    },
    {
        kanji: '仕',
        strokes: 5,
        level: 'N4',
        frequency: 439,
        readings: {
            kunyomi: [
                {
                    'kana': 'つか',
                },
            ],
            onyomi: [
                {
                    'kana': 'シ',
                },
                {
                    'kana': 'ジ',
                },
            ],
        },
        romaji: ['tsuka', 'shi', 'ji'],
        translation: 'servir, travailler',
    },
    {
        kanji: '成',
        strokes: 6,
        level: 'N3',
        frequency: 116,
        readings: {
            kunyomi: [
                {
                    'kana': 'な',
                },
            ],
            onyomi: [
                {
                    'kana': 'セイ',
                },
                {
                    'kana': 'ジョウ',
                },
            ],
        },
        romaji: ['na', 'sei', 'jo', 'jou'],
        translation: 'se transformer, devenir, croissance',
    },
    {
        kanji: '有',
        strokes: 6,
        level: 'N4',
        frequency: 282,
        readings: {
            kunyomi: [
                {
                    'kana': 'あ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ユウ',
                },
                {
                    'kana': 'ウ',
                },
            ],
        },
        romaji: ['a', 'yu', 'yuu', 'u'],
        translation: 'posséder, exister, avoir lieu',
    },
    {
        kanji: '居',
        strokes: 8,
        level: 'N3',
        frequency: 836,
        readings: {
            kunyomi: [
                {
                    'kana': 'い',
                },
                {
                    'kana': 'お',
                },
            ],
            onyomi: [
                {
                    'kana': 'キョ',
                },
                {
                    'kana': 'コ',
                },
            ],
        },
        romaji: ['i', 'o', 'kyo', 'ko'],
        translation: 'exister, se trouver, vivre avec',
    },
    {
        kanji: '良',
        strokes: 7,
        level: 'N3',
        frequency: 501,
        readings: {
            kunyomi: [
                {
                    'kana': 'よ',
                },
                {
                    'kana': 'い',
                },
            ],
            onyomi: [
                {
                    'kana': 'リョウ',
                },
            ],
        },
        romaji: ['yo', 'i', 'ryo', 'ryou'],
        translation: 'bien, plaisant, habile',
    },
    {
        kanji: '授',
        strokes: 11,
        level: 'N1',
        frequency: 523,
        readings: {
            kunyomi: [
                {
                    'kana': 'さず',
                },
            ],
            onyomi: [
                {
                    'kana': 'ジュ',
                },
            ],
        },
        romaji: ['sazu', 'ju'],
        translation: 'instruire, dispenser, conférer',
    },
    {
        kanji: '業',
        strokes: 13,
        level: 'N1',
        frequency: 43,
        readings: {
            kunyomi: [
                {
                    'kana': 'わざ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ギョウ',
                },
                {
                    'kana': 'ゴウ',
                },
            ],
        },
        romaji: ['waza', 'gyo', 'gyou', 'go', 'gou'],
        translation: 'affaire, vocation, art',
    },
    {
        kanji: '供',
        strokes: 8,
        level: 'N3',
        frequency: 313,
        readings: {
            kunyomi: [
                {
                    'kana': 'そな',
                },
                {
                    'kana': 'とも',
                },
                {
                    'kana': 'ども',
                },
            ],
            onyomi: [
                {
                    'kana': 'キョウ',
                },
                {
                    'kana': 'ク',
                },
                {
                    'kana': 'クウ',
                },
                {
                    'kana': 'グ',
                },
            ],
        },
        romaji: ['sona', 'tomo', 'domo', 'kyo', 'kyou', 'ku', 'kuu', 'gu'],
        translation: 'offrir, soumettre, accompagner',
    },
    {
        kanji: '達',
        strokes: 12,
        level: 'N3',
        frequency: 500,
        readings: {
            kunyomi: [
                {
                    'kana': 'たち',
                },
                {
                    'kana': 'だち',
                },
            ],
            onyomi: [
                {
                    'kana': 'タツ',
                },
                {
                    'kana': 'ダ',
                },
            ],
        },
        romaji: ['tachi', 'dachi', 'tatsu', 'da'],
        translation: 'accompli, atteindre, aboutir',
    },
    {
        kanji: '昨',
        strokes: 9,
        level: 'N3',
        frequency: 226,
        readings: {
            kunyomi: [],
            onyomi: [
                {
                    'kana': 'サク',
                },
            ],
        },
        romaji: ['saku'],
        translation: 'hier, précédent',
    },
    {
        kanji: '合',
        strokes: 6,
        level: 'N3',
        frequency: 41,
        readings: {
            kunyomi: [
                {
                    'kana': 'あ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ゴウ',
                },
                {
                    'kana': 'ガッ',
                },
                {
                    'kana': 'カッ',
                },
            ],
        },
        romaji: ['a', 'go', 'gou', 'gatsu', 'katsu'],
        translation: 'correspondre, convenir, joindre',
    },
    {
        kanji: '図',
        strokes: 7,
        level: 'N4',
        frequency: 539,
        readings: {
            kunyomi: [
                {
                    'kana': 'え',
                },
                {
                    'kana': 'はか',
                },
            ],
            onyomi: [
                {
                    'kana': 'ズ',
                },
                {
                    'kana': 'ト',
                },
            ],
        },
        romaji: ['e', 'haka', 'zu', 'to'],
        translation: 'carte, plan, dessin',
    },
    {
        kanji: '美',
        strokes: 9,
        level: 'N3',
        frequency: 462,
        readings: {
            kunyomi: [
                {
                    'kana': 'うつく',
                },
            ],
            onyomi: [
                {
                    'kana': 'ビ',
                },
            ],
        },
        romaji: ['utsuku', 'bi'],
        translation: 'beauté',
    },
    {
        kanji: '味',
        strokes: 8,
        level: 'N4',
        frequency: 442,
        readings: {
            kunyomi: [
                {
                    'kana': 'あじ',
                },
            ],
            onyomi: [
                {
                    'kana': 'ミ',
                },
            ],
        },
        romaji: ['aji', 'mi'],
        translation: 'saveur, goût',
    },
    {
        kanji: '為',
        strokes: 9,
        level: 'N1',
        frequency: 831,
        readings: {
            kunyomi: [
                {
                    'kana': 'ため',
                },
                {
                    'kana': 'な',
                },
                {
                    'kana': 'す',
                },
            ],
            onyomi: [
                {
                    'kana': 'イ',
                },
            ],
        },
        romaji: ['tame', 'na', 'su', 'i'],
        translation: 'faire, bénéfice, bien-être',
    },
    {
        kanji: '等',
        strokes: 12,
        level: 'N3',
        frequency: 798,
        readings: {
            kunyomi: [
                {
                    'kana': 'ひと',
                },
                {
                    'kana': 'など',
                },
                {
                    'kana': 'ら',
                },
            ],
            onyomi: [
                {
                    'kana': 'トウ',
                },
            ],
        },
        romaji: ['hito', 'nado', 'ra', 'to', 'tou'],
        translation: 'semblable, etc, et ainsi de suite',
    },
    {
        kanji: '迄',
        strokes: 6,
        level: null,
        frequency: 2500,
        readings: {
            kunyomi: [
                {
                    'kana': 'まで',
                },
            ],
            onyomi: [],
        },
        romaji: ['made'],
        translation: 'jusqu\'à',
    },
    {
        kanji: '又',
        strokes: 2,
        level: null,
        frequency: 2500,
        readings: {
            kunyomi: [
                {
                    'kana': 'また',
                },
            ],
            onyomi: [],
        },
        romaji: ['mata'],
        translation: 'de plus, ou encore',
    },
];

// Collections:
// 1 - Le petit livre des kanjis, par Kuniko Braghini (150 kanjis)


export default kanjis;